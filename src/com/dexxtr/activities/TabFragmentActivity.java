package com.dexxtr.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.dexxtr.R;
import com.dexxtr.fragments.BaseFragment;
import com.dexxtr.fragments.FragmentA;
import com.dexxtr.fragments.FragmentB;
import com.dexxtr.fragments.FragmentC;
import com.dexxtr.fragments.FragmentD;

public class TabFragmentActivity extends FragmentActivity implements
		OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main);

		setTabClickListener();

		switchTab(new FragmentA());
	}

	public void onClick(View arg0) {
		BaseFragment tab;

		switch (arg0.getId()) {

		case R.id.tabA:
			tab = new FragmentA();
			break;

		case R.id.tabB:
			tab = new FragmentB();
			break;

		case R.id.tabC:
			tab = new FragmentC();
			break;

		case R.id.tabD:
			tab = new FragmentD();
			break;

		default:
			tab = new FragmentA();
			break;
		}

		switchTab(tab);
	}

	private void setTabClickListener() {
		Button btn;

		btn = (Button) findViewById(R.id.tabA);
		btn.setOnClickListener(this);

		btn = (Button) findViewById(R.id.tabB);
		btn.setOnClickListener(this);

		btn = (Button) findViewById(R.id.tabC);
		btn.setOnClickListener(this);

		btn = (Button) findViewById(R.id.tabD);
		btn.setOnClickListener(this);
	}

	private void switchTab(BaseFragment tab) {
		FragmentManager fm = getSupportFragmentManager();
		Fragment fragment = fm.findFragmentById(R.id.fragment_content);

		FragmentTransaction ft = fm.beginTransaction();
		if (fragment == null) {
			ft.add(R.id.fragment_content, tab);
		} else {
			ft.replace(R.id.fragment_content, tab);
		}
		ft.commit();
	}
}